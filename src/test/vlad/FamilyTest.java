package vlad;

import org.junit.*;
import vlad.humans.Family;
import vlad.humans.Human;
import vlad.humans.Man;
import vlad.humans.Woman;

import java.util.List;

import static org.junit.Assert.*;

public class FamilyTest {
    private Woman mother1 = new Woman("Elena", "Lietun");
    private Man father1 = new Man("Vlad", "Lietun");

    private Family family1 = new Family(mother1, father1);
    private Man child1 = new Man("Nick", "Lietun");
    private Man child2 = new Man("Denis", "Lietun");

    private Woman mother2 = new Woman("Kate", "Bubalo");
    private Man father2 = new Man("Nick", "Bubalo");
    private Family family2 = new Family(mother2, father2);

    @Before
    public void setUp() {
        family1.addChild(child1);
        family1.addChild(child2);
    }

    public String getExpectedToString(Family family) {
        String result = "Family{mother=" + family.getMother() +
                ", father=" + family.getFather() +
                ", children=" + family.getChildren() + ", pet=null}";
        return result;
    }

    @Test
    public void testToString(){
        Assert.assertEquals(family1.toString(), getExpectedToString(family1));
    }

    @Test
    public void testDeleteChildPositive() {
        int resultBefore = family1.getChildren().size();
        family1.deleteChild(child2);
        Assert.assertEquals(family1.getChildren().size(), resultBefore - 1);

        for (Human child : family1.getChildren()) {
            Assert.assertNotSame(child, child2);
        }
    }

    @Test
    public void testDeleteChildNegative() {
        List<Human> resultBefore = family1.getChildren();
        family1.deleteChild(new Man("Nick", "Lietu"));
        Assert.assertEquals(resultBefore, family1.getChildren());
    }

    @Test
    public void testDeleteChildIndexPositive() {
        int resultBefore = family1.getChildren().size();
        family1.deleteChild(1);
        assertEquals(1, resultBefore-1);
        assertSame(family1.getChildren().get(0), child1);
    }

    @Test
    public void testDeleteChildIndexNegative() {
        assertEquals(2, family1.getChildren().size());
        assertSame(family1.getChildren().get(0), child1);
    }

    @Test
    public void testAddChildPositive() {
        int resultBefore = family1.getChildren().size();
        Human newChild = new Man("Pavel", "Lietun");
        family1.addChild(newChild);
        Assert.assertEquals(family1.getChildren().size(), resultBefore + 1);
        Assert.assertEquals(family1.getChildren().get(family1.getChildren().size() - 1), newChild);
    }

    @Test
    public void testCountFamily() {
        int resultCount = family2.countFamily();
        family2.addChild(new Man());
        Assert.assertEquals(++resultCount, family2.countFamily());
        family2.addChild(new Man());
        Assert.assertEquals(++resultCount, family2.countFamily());
    }
}
