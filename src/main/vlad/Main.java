package vlad;


import vlad.console.Console;
import vlad.controller.FamilyController;
import vlad.dao.CollectionFamilyDao;
import vlad.humans.Family;
import vlad.humans.Human;
import vlad.humans.Man;
import vlad.humans.Woman;
import vlad.dao.FamilyDao;
import vlad.pets.Dog;
import vlad.pets.Pet;
import vlad.service.FamilyService;

import java.text.ParseException;
import java.util.*;

public class Main {

    public static FamilyService createFamily() {
        Human child1 = new Man("Vlad", "Lietun");
        child1.setBirthDate("16.03.1995");
        Human child2 = new Man("Nick", "Bubalo");
        Human woman = new Woman("Elena", "Lietun", "23.03.1972", 50, null);
        Human man = new Man("Dima", "Lietun", "23.09.1973", 50, null);

        Family family = new Family(woman, man);
        Set<Pet> pets = new HashSet<>();

        Dog tuzik = new Dog("Tuzik", 5, 20, null);
        Dog bobik = new Dog("Bobik", 10, 50, null);

        pets.add(tuzik);
        pets.add(bobik);
        family.addChild(child1);
        family.setPet(pets);

        Human woman1 = new Woman("Kate", "Bubalo");
        Human man1 = new Man("Bob", "Bubalo");
        Family family1 = new Family(woman1, man1);
        family1.addChild(child2);

        FamilyDao familyDao = new CollectionFamilyDao();
        familyDao.saveFamily(family);
        familyDao.saveFamily(family1);

        FamilyService familyService = new FamilyService(familyDao);
        return familyService;
    }

    public static void main(String[] args) {

        Console console = new Console();
        console.consoleRun();
    }
}





